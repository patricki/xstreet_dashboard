import React from "react";
import { Route, Router, BrowserRouter, Redirect } from "react-router-dom";
import App from "./containers/App";
import Loader from "./components/Loader/Loader";
import LoggedOut from "./components/LoggedOut";
import Login from "./components/Login";
import AccountContainer from "./containers/AccountContainer";
import Auth from "./helpers/Auth";
import history from "./history";

const auth = new Auth();

const handleCallback = (nextState, replace) => {
  console.log("NEXT STATE handleAuthentication YOOO", nextState.location.hash);
  if (auth.isAuthenticated()) {
    history.push("/account");
  } else {
    console.log("CB FAIL", auth.isAuthenticated());
  }
};

export const makeMainRoutes = props => {
  return (
    <Router history={history}>
      <div>
        <Route path="/" render={props => <App auth={auth} {...props} />} />
        <Route
          path="/loggedout"
          render={props => <LoggedOut auth={auth} {...props} />}
        />
        <Route
          path="/callback"
          render={props => {
            console.log("route props", props);
            handleCallback(props);
            return <Loader {...props} />;
          }}
        />
        <Route
          path="/login"
          render={props => <Login auth={auth} {...props} />}
        />
      </div>
    </Router>
  );
};

const mapStateToProps = ({ user, ui }) => ({
  user,
  ui
});

//export default withRouter(connect(mapStateToProps)(makeMainRoutes));
