import React from "react";
import { NavLink } from "react-router-dom";
import logo from "../assets/logo.png";
import profile from "../assets/user.svg";
import pw from "../assets/password.svg";
import plan from "../assets/cc.svg";
import support from "../assets/support.svg";
import signout from "../assets/signOut.svg";
import terms from "../assets/terms.svg";
import Auth from "../helpers/Auth";

const auth = new Auth();

function handleLogout(e) {
  e.preventDefault();
  auth.logout();
}

export const Nav = ({ userName }) => (
  <aside>
    <div className="logo">
      <img src={logo} alt="" />
    </div>

    <h3>Welcome, {userName}</h3>
    <nav>
      <NavLink to="/account" activeClassName="selected">
        <img src={plan} alt="" />
        Account Plan
      </NavLink>
      <NavLink to="/password" activeClassName="selected">
        <img src={pw} alt="" />
        Password
      </NavLink>
      <NavLink to="/support" activeClassName="selected">
        <img src={support} alt="" />
        Support
      </NavLink>

      <a onClick={handleLogout}>
        <img src={signout} alt="" />Sign Out
      </a>
      <NavLink to="/tos" activeClassName="selected">
        <img src={terms} alt="" />
        Terms of Service
      </NavLink>
      <NavLink to="/privacy" activeClassName="selected">
        <img src={terms} alt="" />
        Privacy Policy
      </NavLink>
    </nav>
  </aside>
);

export default Nav;

Nav.defaultProps = {
  fname: "Peter"
};
