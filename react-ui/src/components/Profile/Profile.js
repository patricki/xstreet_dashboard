import React, { Component } from "react";
import classNames from "classnames";
import { get, pickBy } from "lodash";

class Profile extends Component {
  state = {
    displayName: "",
    brokerage: "",
    phoneOffice: "",
    phoneCell: "",
    email: "",
    mlsId: "",
    isEditing: false
  };

  toggleEditForm = e => {
    e.preventDefault();
    this.setState({ isEditing: !this.state.isEditing });
  };

  submitForm = () => {
    const newProfile = pickBy(this.state, (val, key) => {
      return val !== "";
    });
    console.log(newProfile);
  };

  onChange = e => {
    const endValue = get(
      e.target,
      "rawValue",
      get(e.target, "value", "")
    ).trim();
    const newState = this.state;
    newState[e.target.name] = endValue;
    return this.setState(newState);
  };
  render() {
    const {
      displayName,
      brokerage,
      phoneCell,
      phoneOffice,
      email,
      mlsId,
      isEditing
    } = this.state;
    const { userProfile } = this.props;
    return (
      <form>
        <div className="container">
          <div className="row profile-section">
            <div className="panel">
              <h2>GENERAL</h2>
              <label htmlFor="displayName">Name</label>
              <input
                type="text"
                name="displayName"
                onChange={this.onChange}
                value={isEditing ? displayName : userProfile.displayName}
                placeholder={userProfile.displayName}
              />
              <label htmlFor="brokerage">Brokerage</label>
              <input
                type="text"
                name="brokerage"
                onChange={this.onChange}
                value={isEditing ? brokerage : userProfile.brokerage}
                placeholder={userProfile.brokerage}
              />
              <label htmlFor="brokerage">MLS ID</label>
              <input
                type="text"
                name="mlsId"
                onChange={this.onChange}
                value={isEditing ? mlsId : userProfile.mlsId}
                placeholder={userProfile.mlsId}
              />
            </div>
            <div className="panel">
              <h2>CONTACT INFO</h2>
              <label htmlFor="displayName">Email</label>
              <input
                type="email"
                name="email"
                onChange={this.onChange}
                value={isEditing ? email : userProfile.email}
                placeholder={userProfile.email}
              />
              <label htmlFor="brokerage">Cell #</label>
              <input
                type="tel"
                name="phoneCell"
                onChange={this.onChange}
                value={isEditing ? phoneCell : userProfile.avenuePhoneNumber}
                placeholder={userProfile.avenuePhoneNumber}
              />
              <label htmlFor="brokerage">Office #</label>
              <input
                type="tel"
                name="phoneOffice"
                onChange={this.onChange}
                value={isEditing ? phoneOffice : userProfile.mlsPhoneNumber}
                placeholder={userProfile.mlsPhoneNumber}
              />
            </div>
          </div>
          {/* row */}
          <div className="row">
            <button
              className={classNames({
                "btn-profile": true,
                "is-blue": isEditing
              })}
              onClick={this.toggleEditForm}
            >
              {isEditing ? "SAVE" : "EDIT"}
            </button>
            {isEditing && (
              <button className="btn-profile" onClick={this.toggleEditForm}>
                CANCEL
              </button>
            )}
          </div>
          {/* row */}
        </div>
      </form>
    );
  }
}

export default Profile;
