import React from "react";
import { convertDate } from "../../helpers/strings";

const PayHistory = ({
  history,
  planCanceled,
  handleChangePayment,
  nextCharge,
  showFullHistory,
  handleHistoryView
}) => {
  return (
    <div className="panel plan-history">
      <h2>payment history</h2>
      <div className="history-table">
        <div className="hist-row head">
          <span>
            <h4>Date</h4>
          </span>
          <h4>Amount</h4>
        </div>
        {history.map((item, i) => (
          <div className="hist-row" key={i}>
            <span>{convertDate(item.date, "MM/DD/YYYY")}</span>
            <em>${item.amount}</em>
          </div>
        ))}
        {history.length >= 3 && (
          <a onClick={handleHistoryView}>
            Show {showFullHistory ? "less" : "all"}
          </a>
        )}
      </div>
      {!planCanceled && (
        <div>
          <em className="next-charge">
            Next payment will be charged on{" "}
            {convertDate(nextCharge, "MM/DD/YYYY")}
          </em>
          <a className="action-link" onClick={handleChangePayment}>
            Change Payment Method
          </a>
        </div>
      )}
    </div>
  );
};

PayHistory.defaultProps = {
  history: [
    {
      date: "08/10/2017",
      amount: "$60"
    },
    {
      date: "07/10/2017",
      amount: "$60"
    },
    {
      date: "06/10/2017",
      amount: "$60"
    }
  ]
};

export default PayHistory;
