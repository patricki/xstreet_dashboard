import React from "react";

const Alert = () => (
  <div className="alert-declined">
    <p>
      There was something wrong with your payment method. Please update your
      payment details with a valid card
    </p>
  </div>
);

export default Alert;
