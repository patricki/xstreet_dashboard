import React, { Component } from "react";
import { connect } from "react-redux";
import { injectStripe } from "react-stripe-elements";
import PlanStatus from "./PlanStatus";
import PayHistory from "./PayHistory";
import PaymentForm from "./PaymentForm";
import Modal from "../Modal";
import Alert from "./Alert";
import { cancelSubscription, reactivateSubscription } from "../../redux/user";
import { get, every, reverse } from "lodash";

class Account extends Component {
  state = {
    modalOpen: false,
    showPaymentForm: false,
    chargeFail: false,
    changePayment: false,
    showFullHistory: false,
    paymentSuccess: null
  };

  componentWillMount() {
    this.checkFailedCharge();
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.user.externalPaymentToken !==
      nextProps.user.externalPaymentToken
    ) {
      this.handlePayConfirm();
      console.log("PROPS CHANGED", nextProps.user.externalPaymentToken);
    }
    console.log("PROPS DID NOT CHANGE");
  }

  handlePayConfirm = () => {
    this.setState({ paymentSuccess: true });
    setTimeout(() => {
      this.setState({
        paymentSuccess: null,
        changePayment: !this.state.changePayment
      });
    }, 4000);
  };

  checkFailedCharge = () => {
    const { user } = this.props;
    if (user.externalPaymentHistory) {
      const hasFailed = every(user.externalPaymentHistory, {
        type: "charge.succeeded"
      });
      this.setState({ chargeFail: !hasFailed });
    }
  };

  affirmCancel = ev => {
    this.props.cancelSubscription();
    this.setState({ modalOpen: false });
  };

  handleCancel = ev => {
    ev.preventDefault();
    this.setState({ modalOpen: true });
  };

  handleChangePayment = ev => {
    ev.preventDefault();
    this.setState({ changePayment: !this.state.changePayment });
  };

  handleReactivate = ev => {
    ev.preventDefault();
    this.props.reactivateSubscription();
  };

  showPaymentForm = ev => {
    this.setState({ showPaymentForm: true });
  };

  handleHistoryView = ev => {
    ev.preventDefault();
    this.setState({ showFullHistory: !this.state.showFullHistory });
  };

  closeModal = () => {
    this.setState({ modalOpen: false });
  };

  render() {
    const {
      modalOpen,
      chargeFail,
      changePayment,
      showFullHistory,
      paymentSuccess
    } = this.state;
    const { user } = this.props;
    const onTrial = get(user, "subscriptionInfo.isTrial", true);
    const planCanceled = get(user, "subscriptionInfo.cancelAtPeriodEnd", null);
    const nextCharge = get(user, "subscriptionInfo.currentPeriodEnd", "--");
    const rawHistory = get(user, "externalPaymentHistory", []);
    const paymentHistory = reverse(
      rawHistory.slice(0, showFullHistory ? -1 : 3)
    );

    return (
      <div className="container">
        <div className="row">
          <h3 className="welcome-title">Welcome, {user.displayName}</h3>
          <div className="panel-col">
            {chargeFail && <Alert />}
            <PlanStatus
              plan={user}
              cancelHandler={this.handleCancel}
              affirmCancel={this.affirmCancel}
              handleReactivate={this.handleReactivate}
              showPaymentForm={this.showPaymentForm}
            />
          </div>
          <div className="panel-col">
            {(onTrial || changePayment) && (
              <PaymentForm
                changePayment={changePayment}
                handleChangePayment={this.handleChangePayment}
                stripe={this.props.stripe}
                paymentSuccess={paymentSuccess}
              />
            )}
            {!onTrial &&
              !changePayment && (
                <PayHistory
                  history={paymentHistory}
                  planCanceled={planCanceled}
                  handleChangePayment={this.handleChangePayment}
                  nextCharge={nextCharge}
                  showFullHistory={showFullHistory}
                  handleHistoryView={this.handleHistoryView}
                />
              )}
          </div>
        </div>
        <Modal isOpen={modalOpen}>
          <div className="modal-panel">
            <div className="modal-panel--content">
              <h2>cancel premium</h2>
              <div className="center-block" style={{ margin: "2rem 0" }}>
                <p>
                  Are you sure you want to cancel your access to Cross Street?
                </p>
              </div>
            </div>
            <div className="btn-action-row">
              <button onClick={this.closeModal}>No</button>
              <button onClick={this.affirmCancel}>Yes</button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = ({ user, ui }) => ({
  user,
  ui
});

const mapDispatchToProps = dispatch => ({
  cancelSubscription: () => dispatch(cancelSubscription()),
  reactivateSubscription: () => dispatch(reactivateSubscription())
});

export default injectStripe(
  connect(mapStateToProps, mapDispatchToProps)(Account)
);
