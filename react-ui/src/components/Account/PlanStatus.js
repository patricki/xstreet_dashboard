import React from "react";
import { convertDate, convertDaysLeft } from "../../helpers/strings";
import { get, noop } from "lodash";
import pill_premium from "../../assets/pill_premium.png";
import pill_trial from "../../assets/pill_trial.png";

function isPremiumPlan(plan) {
  if (plan === "premium" || plan === "basic") {
    return true;
  }
  return false;
}

const TrialDetails = ({ daysRemaining }) => (
  <div className="status-calendar">
    <h2>{convertDaysLeft(daysRemaining)}</h2>
    <p>days remaining</p>
  </div>
);

const PaidDetails = ({ plan }) => (
  <div className="status-calendar">
    <h2>{convertDate(plan.subscriptionInfo.currentPeriodStart, "MMM 'YY")}</h2>
    <p>member since</p>
  </div>
);

const Reactivate = ({ plan, showPaymentForm, reactivate }) => (
  <div className="plan-canceldate">
    <div className="enddate">
      <h4>Ends on</h4>
      <p>{convertDate(plan.subscriptionInfo.currentPeriodEnd, "MM/DD/YYYY")}</p>
    </div>
    <a onClick={reactivate}>Reactivate Premium</a>
  </div>
);

const PlanStatus = ({
  plan,
  cancelHandler,
  showPaymentForm,
  handleReactivate
}) => {
  const planCancelled = get(plan, "subscriptionInfo.cancelAtPeriodEnd", false);
  const onTrial = get(plan, "subscriptionInfo.isTrial", true);
  const daysLeft = get(plan, "subscriptionInfo.cancelTrialAtPeriodEnd", 30);
  return (
    <div className="panel">
      <h2>current plan</h2>
      <div className="plan-details">
        <img src={onTrial ? pill_trial : pill_premium} alt="" />
        {onTrial ? (
          <TrialDetails daysRemaining={daysLeft} />
        ) : (
          <PaidDetails plan={plan} />
        )}
      </div>
      {!onTrial &&
        planCancelled && (
          <Reactivate
            plan={plan}
            reactivate={handleReactivate}
            showPaymentForm={showPaymentForm}
          />
        )}
      {!onTrial &&
        !planCancelled && (
          <a className="action-link" onClick={cancelHandler}>
            Cancel Premium
          </a>
        )}
    </div>
  );
};

PlanStatus.defaultProps = {
  plan: {},
  cancelHandler: noop,
  showPaymentForm: noop,
  handleReactivate: noop
};

export default PlanStatus;
