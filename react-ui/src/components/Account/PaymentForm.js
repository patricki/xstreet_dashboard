/* global Stripe */
import React, { Component } from "react";
import classNames from "classnames";
import { CardElement } from "react-stripe-elements";
import { sendStripePayment } from "../../helpers/http";
import success from "../../assets/success.svg";

class PaymentForm extends Component {
  state = {
    stripeTokenId: null,
    planTier: "annual"
  };

  handleSubmit = ev => {
    ev.preventDefault();
    this.props.stripe.createToken().then(payload => {
      console.log("STRIPE PYMT", payload);
      sendStripePayment(payload.token.id, this.state.planTier);
    });
  };

  togglePrice = planTier => {
    this.setState({ planTier: planTier });
  };

  render() {
    const { planTier } = this.state;
    const { changePayment, handleChangePayment } = this.props;
    const priceTitle = planTier === "annual" ? "$50" : "$60";
    const createOptions = fontSize => {
      return {
        style: {
          base: {
            fontSize: "18px",
            color: "#424770",
            letterSpacing: "0.025em",
            fontFamily: "Source Code Pro, Menlo, monospace",
            "::placeholder": {
              color: "#aab7c4"
            }
          },
          invalid: {
            color: "#9e2146"
          }
        }
      };
    };
    return (
      <div className="panel">
        <h2>{changePayment ? "change payment" : "activate premium"}</h2>
        <div className="pricing-tab">
          <button
            className={classNames({
              "pricing-tab--selector": true,
              "is-active": this.state.planTier === "annual"
            })}
            onClick={() => this.togglePrice("annual")}
          >
            Annual
          </button>
          <button
            className={classNames({
              "pricing-tab--selector": true,
              "is-active": this.state.planTier === "premium"
            })}
            onClick={() => this.togglePrice("premium")}
          >
            Monthly
          </button>
        </div>
        <div className="price-title">
          <h1>
            {priceTitle}
            <span>/mo</span>
          </h1>
          {planTier === "annual" && <p>Billed Annually</p>}
        </div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Card details
            <CardElement {...createOptions(this.props.fontSize)} />
          </label>
          <div className="inline-buttons">
            <button className="btn-pay" onClick={this.handleSubmit}>
              {changePayment ? "UPDATE CARD" : "PAY"}
            </button>
            {changePayment && (
              <button className="btn-cancel" onClick={handleChangePayment}>
                Cancel{" "}
              </button>
            )}
          </div>
        </form>
        <div
          className={classNames({
            "payment-confirm": true,
            "is-active": this.props.paymentSuccess
          })}
        >
          <img src={success} alt="" />
          <h3>Success! Your payment went through</h3>
        </div>
      </div>
    );
  }
}

export default PaymentForm;
