import React from "react";
import { NavLink } from "react-router-dom";
import classNames from "classnames";
import profile from "../assets/user.svg";
import pw from "../assets/password.svg";
import plan from "../assets/cc.svg";
import support from "../assets/support.svg";
import terms from "../assets/terms.svg";
import signout from "../assets/signOut.svg";
import Auth from "../helpers/Auth";

const auth = new Auth();

function handleLogout(e) {
  e.preventDefault();
  auth.logout();
}

const Drawer = ({ drawerOpen, toggleMobileDrawer }) => {
  return (
    <div
      className={classNames({
        drawer: true,
        "is-open": drawerOpen
      })}
    >
      <nav>
        <NavLink
          to="/account"
          activeClassName="selected"
          onClick={toggleMobileDrawer}
        >
          <img src={plan} alt="" />
          Account Plan
        </NavLink>
        <NavLink
          to="/password"
          activeClassName="selected"
          onClick={toggleMobileDrawer}
        >
          <img src={pw} alt="" />
          Password
        </NavLink>
        <NavLink
          to="/support"
          activeClassName="selected"
          onClick={toggleMobileDrawer}
        >
          <img src={support} alt="" />
          Support
        </NavLink>
        <a onClick={handleLogout}>
          <img src={signout} alt="" />Sign Out
        </a>
        <NavLink
          to="/tos"
          activeClassName="selected"
          onClick={toggleMobileDrawer}
        >
          <img src={terms} alt="" />
          Terms of Service
        </NavLink>
        <NavLink
          to="/privacy"
          activeClassName="selected"
          onClick={toggleMobileDrawer}
        >
          <img src={terms} alt="" />
          Privacy Policy
        </NavLink>
      </nav>
    </div>
  );
};

export default Drawer;
