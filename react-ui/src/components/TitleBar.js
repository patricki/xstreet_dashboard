import React from "react";

export const rederPageTitle = path => {
  const trimmed = path.substr(1);
  switch (true) {
    case trimmed === "profile":
      return "Profile";
      break;
    case trimmed === "account":
      return "Account Plan";
      break;
    case trimmed === "support":
      return "Support";
      break;
    case trimmed === "password":
      return "Password";
      break;
    case trimmed === "tos":
      return "Terms of Use";
      break;
    case trimmed === "privacy":
      return "Privacy Policy";
      break;
    default:
      return "";
  }
};

export const TitleBar = ({ location }) => (
  <div className="title-bar">
    <h1>{rederPageTitle(location.pathname)}</h1>
  </div>
);

export default TitleBar;
