import React from "react";

const Support = () => (
  <div className="container">
    <div className="row">
      <div className="panel wide">
        <h2>contact Support</h2>

        <div className="center-block" style={{ marginTop: "1.5rem" }}>
          <div className="center-block">
            <a
              className="btn-block"
              style={{ marginTop: "1.5rem" }}
              href="mailto: hello@crossstreet.ai"
              target="_blank"
              rel="noopener noreferrer"
            >
              Contact Support
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Support;
