import React from "react";
import classNames from "classnames";

const Modal = ({ children, isOpen }) => (
  <div className={classNames({ modal: true, "is-open": isOpen })}>
    {React.cloneElement(children, { ...this.props })}
  </div>
);

export default Modal;
