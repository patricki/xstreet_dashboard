import React from "react";
import classNames from "classnames";
import logo from "../assets/logo.png";

const MobileHeader = ({ drawerOpen, toggleMobileDrawer }) => {
  return (
    <header className="mobile-header">
      <button
        className={classNames({
          "hamburger hamburger--slider": true,
          "is-active": drawerOpen
        })}
        type="button"
        onClick={toggleMobileDrawer}
      >
        <span className="hamburger-box">
          <span className="hamburger-inner" />
        </span>
      </button>
      <img src={logo} alt="" />
      <div />
    </header>
  );
};

export default MobileHeader;
