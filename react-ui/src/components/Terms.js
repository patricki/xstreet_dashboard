import React from "react";
import { Link } from "react-router-dom";

const Terms = () => (
  <section className="terms">
    <h2>1. Introduction.</h2>
    <p>
      Welcome to the Cross Street platform, a software and services platform
      that manages scheduling, routing, and messaging activity between real
      estate agents and their clients regarding the sale or rental of real
      estate properties.
      <strong>
        THIS AGREEMENT BINDS YOU (“YOU,” OR “YOUR”) TO THE TERMS AND CONDITIONS
        SET FORTH HEREIN IN CONNECTION WITH YOUR USE OF AVENUE LABS, INC.’S
        (“OUR”, “WE”, OR “COMPANY”) SERVICES, WEBSITES, MOBILE APPLICATIONS,
        ONLINE APPLICATIONS OR RELATED TOOLS, SOFTWARE OR OTHER OFFERINGS
        PROVIDED BY US (COLLECTIVELY, THE “PLATFORM”). BY ACCESSING OR USING ANY
        OF THE PLATFORM, YOU AGREE TO BE BOUND BY THE TERMS AND CONDITIONS OF
        THIS AGREEMENT. IF YOU DO NOT AGREE TO ALL THE TERMS AND CONDITIONS OF
        THIS AGREEMENT, PLEASE DO NOT ACCESS THE PLATFORM. OUR ACCEPTANCE IS
        EXPRESSLY CONDITIONED ON YOUR AGREEMENT TO ALL THE TERMS AND CONDITIONS
        OF THIS AGREEMENT.
      </strong>
    </p>

    <h2>2. Users/Registration.</h2>
    <p>
      The Platform allows users who are real estate agents and brokers to
      connect with and communicate with other real estate agents and brokers and
      registration and use of the Platform is restricted to individuals
      (“Users") who are at least 18 years of age or older and who are a licensed
      real estate agent or real estate broker. By registering to use the
      Platform, you represent that you are at least 18 years of age or older and
      either a licensed real estate agent or broker in good standing. If you do
      not meet all of these requirements, you must not access or use the
      Platform.
    </p>
    <p>
      To use the Platform, you will need to register and obtain an account, a
      login name and password (“Credentials") which are unique to you. When you
      register, the information you provide to us during the registration
      process will help us in offering content, customer service and network
      management. It is a condition of your use of the Platform that all the
      information you provide on the Platform is correct, current and complete.
      You are solely responsible for the security of your Credentials and may
      not share them with any other person. If you believe that someone has
      stolen or otherwise used your Credentials, you must let us know
      immediately. You will be responsible for any use of the Platform by anyone
      using your Credentials. You are solely responsible for all telephony, data
      charges and/or other fees and costs associated with your access to and use
      of the Platform, as well as for obtaining and maintaining all telephone,
      computer hardware, and other equipment required for such access and use.
    </p>
    <p>
      We have the sole discretion to accept or reject any registrations by any
      persons. Also, we can terminate a User at any time for any or no reason.
      In addition, at any time, in our sole discretion and without notice to
      you, we can suspend or terminate your Credentials and your right to access
      the Platform. We likewise can delete any User Content (as defined below)
      in our sole discretion at any time without notice to you. You agree that
      no claim or cause of action will arise from us exercising any of our
      rights under these Terms of Use.
    </p>

    <h2>3. Changes to the Platform. </h2>
    <p>
      We reserve the right to withdraw or amend the Platform, and any material,
      content, functionality and services offered on or through the Platform, in
      our sole discretion without notice. We have no obligation to update,
      upgrade or fix problems with respect to the Platform. We will not be
      liable if for any reason all or any part of the Platform is unavailable at
      any time or for any period and provide no assurance that the Platform will
      meet your requirements, be error free or operate on an error free basis.
      From time to time, we may restrict access to some parts of the Platform,
      or the entire Platform, to Users. You are responsible for making all
      arrangements necessary for you to have access to the Platform.
    </p>

    <h2>4. Obligations of Users Using the Platform.</h2>
    <p>You represent, warrant and agree that:</p>
    <ul>
      <li>
        You will use any multiple listing service (“MLS”) data or content
        contained on the Platform in accordance with the rules and regulations
        of the applicable MLS agency and not copy or download any such data.
      </li>
      <li>
        You may communicate and schedule with users outside of the Platform, and
        inside or outside of your MLS; you are responsible for who you schedule
        with, and the data that is provided to them.
      </li>
      <li>
        As part of the Platform, you agree that we may use a variety of methods
        to schedule appointments and message with third-parties on your behalf,
        including but not limited to calls, texts and email.
      </li>
      <li>
        In using the Platform you will comply with all anti-spam and other laws
        governing electronic communications.
      </li>
      <li>
        You are responsible for ensuring that all communications are addressed
        to the proper person. You understand that messages can in all events end
        up being received by or shown to someone other than the intended
        recipient, including sellers, tenants and landlords.
      </li>
      <li>
        By responding to a request for scheduling through the Platform, even if
        you are not a User, your contact details will be recorded, and we will
        use such details to schedule future showings with you and keep a record
        and to keep a history of your use of the Platform in the event that you
        become a User.
      </li>
      <li>
        You can receive SMS Text marketing messages via an automatic telephone
        dialing system from us.
      </li>
      <li>
        We and our affiliates can use any information obtained from your use of
        the Platform to market our products and services as well as those of our
        affiliates.
      </li>
    </ul>

    <h2>5. Prohibited Uses.</h2>
    <p>
      You may use the Platform only for lawful purposes and in accordance with
      these Terms of Use. You are solely responsible for the knowledge of and
      adherence to any and all laws, rules, and regulations pertaining to your
      use of the Platform and the content contained therein. You assume any and
      all risks from any meetings or contact between you and any other user of
      the Platform. You agree not to do any of the following:
    </p>
    <ul>
      <li>
        To engage in any conduct that restricts or inhibits anyone's use or
        enjoyment of the Platform, or which may harm us or Users of the Platform
        or expose them to liability.
      </li>
      <li>
        Take any action that violates any applicable federal, state, local or
        international law or regulation (including, without limitation, any laws
        relating to copyright and other intellectual property use, privacy and
        personal identity laws, laws regarding the export of data or software to
        and from the US or other countries and applicable MLS regulations).
      </li>
      <li>
        Take any action for the purpose of exploiting, harming or attempting to
        exploit or harm minors in any way by exposing them to inappropriate
        content, asking for personally identifiable information or otherwise.
      </li>
      <li>
        To distribute, modify, reverse engineer, deface, tarnish, mutilate, or
        hack any portion of the Platform or data in the Platform, or introduce
        any virus, worm, spyware or any other computer code, file or program
        that may or is intended to damage or hijack the operation of any
        hardware, software or telecommunications equipment, or any other aspect
        of the Platform.
      </li>
      <li>
        Translate, adapt or otherwise create derivative works or improvements to
        the Platform.
      </li>
      <li>
        Remove, alter or obscure any trademarks or any copyright, trademark,
        patent or other intellectual property or proprietary rights notices.
      </li>
      <li>
        Remove, disable, circumvent or otherwise create or implement any
        workaround to any copy protection, rights management or security
        features of the Platform.
      </li>
      <li>
        To send, knowingly receive, upload, download, use or re-use any material
        which does not comply with the Content Standards set out in these Terms
        of Use.
      </li>
      <li>
        To transmit, or procure the sending of, any advertising or promotional
        material without our prior written consent.
      </li>
      <li>
        To impersonate or attempt to impersonate us, our employee’s, another
        user or any other person or entity (including, without limitation, by
        using e-mail addresses or screen names associated with any of the
        foregoing).
      </li>
      <li>
        To allow any robot, spider or other automatic device, process or means
        to access the Platform for any purpose.
      </li>
      <li>
        To monitor or copy any of the content or material on the Platform or for
        any other unauthorized purpose without our prior written consent.
      </li>
      <li>
        To collect or store personal data of any other User without the prior,
        written permission of such User.
      </li>
    </ul>

    <h2>6. Content, Licenses and Permissions.</h2>
    <p>
      As part of the use of the Platform, you may post or transmit information,
      text, messages, opinions, commentary, advice, images, photos, video and
      audio or any other materials (“User Content.") Such User Content, can
      include, without limitation:
    </p>
    <ul>
      <li>
        information regarding appointments such as times, dates, agents, contact
        information and property locations;
      </li>
      <li>
        opinions, messages or information regarding properties and
        communications transmitted through the Platform;
      </li>
      <li>
        Responses to surveys or feedback regarding the Platform (“User
        Feedback”).
      </li>
    </ul>
    <p>
      Content remains the proprietary property of the person or entity supplying
      it (or their affiliated and/or third-party providers and suppliers) and is
      protected pursuant to U.S. and foreign copyright and other intellectual
      property laws. You represent and warrant that you have all licenses,
      rights, consents, and permissions necessary to grant the rights set forth
      in these Terms of Use to us with respect to your submitted User Content
      and that we shall not need to obtain any licenses, rights, consents, or
      permissions from, or make any payments to, any third party for any use or
      exploitation of your submitted User Content as authorized in these Terms
      of Use or have any liability to you or any other party as a result of any
      use or of your submitted User Content as authorized in these Terms. You
      may not post or distribute User Content in violation of the rights of
      others, these Terms of Use, the Content Standards, or any applicable laws,
      rules or regulations, including without limitation the regulations and
      rules of your MLS association.
    </p>
    <p>
      You hereby grant the Company, an irrevocable, perpetual, royalty-free,
      worldwide, assignable and sublicensable right to the User Content as it
      determines in its sole discretion. You understand that this license covers
      any use, display or distribution of the User Content in any medium (now
      existing or developed in the future) and includes the right to incorporate
      User Content into new derivative products. You have no “moral rights” with
      respect to the User Content or any right to be compensated for any use of
      User Content. We are not responsible, or liable to any third party, for
      the content or accuracy of any User Content posted by you or any other
      Users of the Platform.
    </p>

    <h2>7. Allowable Content.</h2>
    <p>
      The following content standards ("Content Standards") apply to all User
      Content and use of the Platform. Content must comply with all applicable
      federal, state, local and international laws and regulations. Without
      limiting the foregoing, Content must not:
    </p>
    <ul>
      <li>
        Contain any material which is defamatory, obscene, indecent, abusive,
        offensive, harassing, violent, hateful, inflammatory or otherwise
        objectionable;
      </li>
      <li>
        Promote sexually explicit or pornographic material, violence, or
        discrimination based on race, sex, religion, nationality, disability,
        sexual orientation or age;
      </li>
      <li>Violate Fair Housing Rules;</li>
      <li>
        Infringe any patent, trademark, trade secret, copyright or other
        intellectual property or other rights of any other person;
      </li>
      <li>
        Violate the legal rights (including the rights of publicity and privacy)
        of others or contain any material that could give rise to any civil or
        criminal liability under applicable laws or regulations or that
        otherwise may be in conflict with these Terms of Use and our{" "}
        <a href="privacy.html">Privacy Policy</a>;
      </li>
      <li>Be deceptive;</li>
      <li>
        Promote any illegal activity, or advocate, promote or assist any
        unlawful act;
      </li>
      <li>Involve sweepstakes, lotteries or similar promotions; or</li>
      <li>
        Give the impression that they emanate from or are endorsed by us or any
        other person or entity, if this is not the case.
      </li>
    </ul>

    <h2>8. Third Party Materials.</h2>
    <p>
      The Platform may display, include, or make available third-party content
      (including data, information, applications, and other products, services,
      and/or materials) or provide links to third-party websites or services,
      including through third-party advertising ("Third-Party Materials"). You
      acknowledge and agree that we are not responsible for Third-Party
      Materials, including their accuracy, completeness, timeliness, validity,
      copyright compliance, legality, decency, quality, or any other aspect
      thereof. We do not assume and will not have any liability or
      responsibility to you or any other person or entity for any Third-Party
      Materials. Third-Party Materials and links thereto are provided solely as
      a convenience to you, and you access and use them entirely at your own
      risk and subject to such third parties' terms and conditions. All MLS data
      or content must be used in accordance with applicable MLS rules and
      regulations and may not be copied or downloaded.
    </p>

    <h2>9. Payments.</h2>
    <p>
      If you elect to access or use the Platform that involves payment of a fee,
      then you agree to pay, and will be responsible for payment of, that fee
      and all taxes associated with such access or use. If you provide credit
      card information to pay for such fees then you hereby represent and
      warrant that you are authorized to supply such information and hereby
      authorize us to charge your credit card on a regular basis to pay the fees
      as they are due. Unless otherwise stated, all fees are quoted in U.S.
      Dollars. If your payment method fails or your account is past due, then we
      may collect a late payment fee of 1% per month or the maximum amount
      permitted by law, whichever is greater. We may also collect fees owed
      using other collection mechanisms. This may include charging other payment
      methods on file with us and/or retaining collection agencies and legal
      counsel. We may also block your access to the Platform pending resolution
      of any amounts due by you to us.
    </p>

    <h2>10. Collection of Your Information.</h2>
    <p>
      You acknowledge that when using the Platform we may use automatic means
      (including for example, cookies and web beacons) to collect information
      from you about your mobile device and your use of the Platform, including,
      without limitation, information regarding (a) the devices used by you to
      access the Platform and your IP address, (b) the operating systems and
      software used by your devices, (c) the geographic location of you when
      accessing the Platform, (d) pages accessed, links clicked and persons
      communicated with and Third Party Content accessed (as defined below. You
      may also be required to provide certain information about yourself as a
      condition to using the Platform, including your name, email address,
      postal address phone number, MLS identification number, real estate
      license number.. All such information is used by us in accordance with our{" "}
      <a href="privacy.html">Privacy Policy</a> and you consent to all actions
      we take with respect to your information consistent with our{" "}
      <a href="privacy.html">Privacy Policy</a>.
    </p>

    <h2>11. Monitoring.</h2>
    <p>
      We have the right to: (i) Monitor, log and audit all use of the Platform
      and share any resulting information or communication with any MLS,
      brokerage or government agency as we determine to be necessary or
      appropriate in our sole discretion; (ii) Remove or refuse to post, publish
      distribute and otherwise disclose any User Content for any or no reason in
      our sole discretion; (iii) Take any action with respect to any User
      Content that we deem necessary or appropriate in our sole discretion,
      including if we believe that such User Content violates the Terms of Use,
      including the Content Standards, threatens the personal safety of users of
      the Platform or the public or could create liability for us; (iv) Disclose
      your identity or other information about you to any third party who claims
      that material posted by you violates their rights, including their
      intellectual property rights or their right to privacy; (v) Take
      appropriate legal action, including without limitation, referral to law
      enforcement, for any illegal or unauthorized use of the Platform; (vi)
      Without limiting the foregoing, we have the right to fully cooperate with
      any law enforcement authorities or court order requesting or directing us
      to disclose the identity or other information of anyone posting any
      materials on or through the Platform.
    </p>
    <p>
      The Company has no liability for any action or inaction regarding
      transmissions, communications of Content provided by any User or any third
      party. The Company is under no obligation to investigate any listing or
      other information included as part of the Content and in no event will it
      have any liability or responsibility to anyone for performance or
      nonperformance of the activities described in this section.
    </p>

    <h2>12. Intellectual Property Rights.</h2>
    <p>
      You acknowledge and agree that the Platform is provided under license and
      is not sold to you. You do not acquire any ownership interest in the
      Platform or any other rights other than to use the Platform in accordance
      with these Terms of Use. We use third party content ("Third Party
      Content") licensed or otherwise acquired by us in the Platform. Third
      Party Content includes intellectual property displayed, performed or used
      on the Platform, including without limitation, video, images, logos,
      graphics, music, written or spoken words, and software. We and the
      licensors of the Third Party Content shall retain their entire, right,
      title and interest in and to the Platform (as well as the organization and
      layout of the Platform), including all copyrights, trademarks and other
      intellectual property rights therein, or relating thereto, as applicable.
      You may only use the Platform as expressly contemplated by these Terms of
      Use.
    </p>

    <h2>13. Copyright Infringement.</h2>
    <p>
      The Digital Millennium Copyright Act (the “DMCA”) provides recourse for
      copyright owners who believe that material appearing on the Internet
      infringes their rights under U.S. copyright law. If you believe in good
      faith that any Content infringes your copyright, you (or your agent) may
      send us a notice requesting that the material be removed, or access to it
      blocked. The notice must include the following information: (a) a physical
      or electronic signature of a person authorized to act on behalf of the
      owner of an exclusive right that is allegedly infringed; (b)
      identification of the copyrighted work claimed to have been infringed (or
      if multiple copyrighted works are covered by a single notification, a
      representative list of such works); (c) identification of the material
      that is claimed to be infringing or the subject of infringing activity,
      and information reasonably sufficient to allow us to locate the material;
      (d) the name, address, telephone number, and email address (if available)
      of the complaining party; (e) a statement that the complaining party has a
      good faith belief that use of the material in the manner complained of is
      not authorized by the copyright owner, its agent, or the law; and (f) a
      statement that the information in the notification is accurate and, under
      penalty of perjury, that the complaining party is authorized to act on
      behalf of the owner of an exclusive right that is allegedly infringed.
    </p>
    <p>
      Notices must meet the then-current statutory requirements imposed by the
      DMCA; see http://www.loc.gov/copyright for details. Notices and
      counter-notices should be sent to: Avenue Labs, Inc., 124 E. Olive Ave,
      Burbank 91502, Attn: Peter F. Khoury. We suggest that you consult your
      legal advisor before filing a notice. Also, be aware that there can be
      penalties for false claims under the DMCA. It is our policy to terminate
      the user accounts of repeat infringers.
    </p>

    <h2>14. Disclaimer of Warranty; Limitation of Liability.</h2>
    <p>
      <strong>DISCLAIMER OF WARRANTIES.</strong>
    </p>
    <p>
      THE “PLATFORM”, ALL CONTENT ACCESSED THROUGH THE PLATFORM AND ALL SERVICES
      PROVIDED BY US IN CONNECTION WITH THE PLATFORM ARE PROVIDED ON AN "AS IS,"
      "WITH ALL FAULTS" AND ON AN "AS AVAILABLE" BASIS, WITHOUT WARRANTY OF ANY
      KIND. TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, ALL WARRANTIES
      (EXPRESS OR IMPLIED) ARE HEREBY DISCLAIMED BY US, OUR AFFILIATES AND OUR
      LICENSORS AND SERVICE PROVIDERS. IN FURTHERANCE OF THE FOREGOING WAIVER,
      EACH USER HEREBY ACKNOWLEDGES AND AGREES THAT WE, ON OUR OWN BEHALF AND
      BEHALF OF OUR AFFILIATES, AND OUR LICENSORS AND SERVICE PROVIDERS HAVE
      DISCLAIMED ALL WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
      PURPOSE, NON-INFRINGEMENT AND WARRANTIES THAT MAY ARISE OUT OF COURSE OF
      PERFORMANCE, USAGE OR TRADE PRACTICE WITH RESPECT TO THE PLATFORM AND ALL
      CONTENT AVAILABLE ON OR THROUGH THE PLATFORM. WE MAKE NO WARRANTY OR
      REPRESENTATION OF ANY KIND THAT THE PLATFORM WILL MEET YOUR REQUIREMENTS,
      ACHIEVE ANY INTENDED RESULTS OR BE COMPATIBLE OR WORK WITH ANY OTHER
      APPLCIATIONS OR SERVICES OR OPERATE WITHOUT INTERRUPTION OR BE ERROR FREE.
    </p>
    <p>
      <strong>LIMITATION OF LIABILITY.</strong>
    </p>
    <p>
      TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL WE OR
      OUR AFFILIATES, AND OUR LICENSORS AND SERVICE PROVIDERS HAVE ANY LIABILITY
      TO YOU FOR ANY DAMAGES, CLAIMS OR CAUSES OF ACTIONS ARISING FROM YOUR USE
      OF THE PLATFORM, INABILITY TO USE THE PLATFORM, LOSS OF DATA, INCLUDING,
      WITHOUT LIMITATION, YOUR POSTING OR ACCESSING ANY CONTENT ON OR THROUGH
      THE PLATFORM. YOUR SOLE REMEDY WITH RESPECT TO ANY CLAIMS, DEFECTS OR
      DISPUTES INVOLVING US, THE PLATFORM OR ANY CONTENT ACCESSED ON OR THROUGH
      THE PLATFORM IS TO STOP USING THE PLATFORM. UNDER NO CIRCUMSTANCES, AND
      REGARDLESS OF THE NATURE OF THE CLAIM (INCLUDING TORT), WILL WE BE LIABLE
      FOR ANY INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR
      EXEMPLARY DAMAGES, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH
      CLAIM. THE FOREGOING NOTWITHSTANDING, IF IN ANY JURISDICTION THE FOREGOING
      LIMITATIONS ARE NOT GIVEN FULL EFFECT, YOU AGREE THAT, TO THE FULLEST
      EXTENT PERMITTED BY APPLICABLE LAW, (A) ANY CLAIM MUST BE BROUGHT BY YOU
      IN SUCH JURISDICTION WITHIN ONE (1) YEAR OF ITS ACCRUAL OR IT IS WAIVED
      AND (B) THE MAXIMUM AMOUNT PAYABLE TO YOU, REGARDLESS OF THE NATURE OF THE
      CLAIM (E.G., TORT OR CONTRACT) SHALL NOT EXCEED THE LESSER OF $10,000 OR
      THE AMOUNT PAID BY YOU TO US DURING THE ONE (1) YEAR PERIOD IMMEDIATELY
      PRECEDING THE DATE THAT THE APPLICABLE CLAIM ACCRUED.
    </p>
    <p>
      YOU ACKNOWLEDGE AND AGREE THAT THE WARRANTY DISCLAIMERS AND THE
      LIMITATIONS OF LIABILITY SET FORTH IN THESE TERMS OF USE REFLECT A
      REASONABLE AND FAIR ALLOCATION OF RISK BETWEEN YOU AND US, AND THAT THESE
      LIMITATIONS ARE AN ESSENTIAL BASIS TO OUR ABILITY TO MAKE THE PLATFORM
      AVAILABLE TO YOU ON AN ECONOMICALLY FEASIBLE BASIS.
    </p>

    <h2>15. Indemnification.</h2>
    <p>
      You agree to defend, indemnify and hold harmless us, our affiliates,
      licensors and service providers, and our and their respective officers,
      directors, shareholders, members, employees, contractors, agents,
      licensors, suppliers, successors and assigns from and against any claims,
      liabilities, damages, judgments, awards, losses, costs, expenses or fees,
      including reasonable attorneys' fees, arising out of or relating to your
      breach of any representation or warranty contained herein, your violation
      of these Terms of Use or your use of the Platform, including, but not
      limited to, your User Content, any use of the Platform’s content, services
      and products.
    </p>

    <h2>16. Trademarks.</h2>
    <p>
      Our name, our Company logo and all related names, logos, product and
      service names, designs and slogans are trademarks of the Company or its
      affiliates or licensors. You must not use such marks without our prior
      written permission. All other names, logos, product and service names,
      designs and slogans on this Platform are the trademarks of their
      respective owners.
    </p>

    <h2>17. Miscellaneous.</h2>
    <ol>
      <li>
        The provisions of these Terms of Use addressing disclaimers of
        representations and warranties, limitation of liability, indemnity
        obligations, intellectual property, and governing law shall survive the
        termination of these Terms of Use.
      </li>
      <li>
        These Terms of Use constitute the entire agreement between us and you
        regarding the subject matter of these Terms of Use, and supersede all
        previous written or oral agreements.
      </li>
      <li>
        The Terms of Use shall be governed by and construed in accordance with
        the laws of the State of California, without regard to its conflict of
        laws rules. You expressly agree that the exclusive jurisdiction for any
        claim or dispute under the Terms of Use and or your use of the Platform
        resides in the state and federal courts located in Los Angeles,
        California, and you further expressly agree to submit to the personal
        jurisdiction of such courts for the purpose of litigating any such claim
        or action. No waiver by either party of any breach or default hereunder
        shall be deemed to be a waiver of any preceding or subsequent breach or
        default. The section headings used herein are for convenience only and
        shall not be given any legal import.
      </li>
      <li>
        If any provision of these Terms of Use is found to be illegal, void or
        unenforceable, then that provision shall be deemed severable from these
        Terms and shall not affect the validity and enforceability of any
        remaining provisions of these Terms.
      </li>
      <li>
        We may revise and update these Terms of Use and our{" "}
        <Link to="/privacy">Privacy Policy</Link> from time to time in our sole
        discretion. All changes are effective immediately when we post them, and
        apply to all access to and use of the Platform thereafter. Your
        continued use of the Platform following the posting of revised Terms of
        Use means that you accept and agree to the changes. You are expected to
        check this page so you are aware of any changes, as they are binding on
        you. If at any time you do not wish to be bound by these Terms of Use
        and our <Link to="/privacy">Privacy Policy</Link>, you must discontinue
        using and accessing the Platform. If you continue to use the Platform
        you will be deemed to accept and agree to our current Terms of Use and{" "}
        <Link to="/privacy">Privacy Policy</Link>.
      </li>
      <li>
        If you respond to our surveys or contact us with information including,
        without limitation, feedback data (e.g., questions, comments,
        suggestions or the like) regarding the Platform, the content of the
        Platform or any item on the Platform any User Feedback shall be deemed
        to be non-confidential and we shall have no obligation to you of any
        kind with respect to the User Feedback. Any survey data you provide to
        us (including responses in response to individual properties) can be
        shared with others and will constitute User Feedback. In addition, you
        agree and acknowledge that we are free to reproduce, use, disclose,
        display, exhibit, transmit, perform, create derivative works and
        distribute the User Feedback to others without limitation, and to
        authorize others to do the same. Further, we shall be free to use any
        ideas, concepts, know-how or techniques contained in the User Feedback
        for any purpose whatsoever, including, without limitation, developing,
        manufacturing and marketing products and other items incorporating the
        User Feedback. We shall not be liable or owe any compensation for the
        use or disclosure of the User Feedback.
      </li>
      <li>
        By using the Platform, you acknowledge that we are not a real estate
        broker, mortgage broker or mortgage lender and we do not aid or assist
        home buyers, renters, landlords, or sellers in the transaction to buy,
        sell, lease, or rent real property.
      </li>
      <li>
        The process of scheduling a home showing may involve the communication,
        to or from, of secure and confidential information relating to the home
        being shown. Communication of such information may be through any of
        several methods of communication including, but not limited to,
        telephone, email, text message, mobile push notices, or the Company’s
        website- Is this applicable. You agree to protect all such secure and
        confidential information and to not divulge such information to any
        inappropriate party. Furthermore, you agree to notify us immediately if
        any such secure and confidential information is inadvertently or
        potentially lost, stolen by, or transferred to any unknown and/or
        inappropriate party.
      </li>
    </ol>

    <h2>18. Comments, Concerns and Requests.</h2>
    <p>
      The Platform is operated by Avenue Labs, Inc., 124 E. Olive Ave, CA 91502.
      All feedback, comments, requests and other communications relating to the
      Platform should be directed to{" "}
      <a href="mailto:team@crossstreet.ai">team@crossstreet.ai</a>
    </p>

    <p>Last modified: 6/13/2017</p>
  </section>
);

export default Terms;
