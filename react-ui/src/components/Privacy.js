import React from "react";

const Privacy = () => (
  <section className="terms">
    <div className="term-title">
      <h3>Avenue Labs, Inc. Privacy Policy</h3>
      <p>
        <em>Revised June 13, 2017</em>
      </p>
    </div>
    <p>
      We are Avenue Labs, Inc. located at 124 E. Olive Ave, Burbank, CA 91502
      (referred to as "Company," "we," “our” or "us".) We are committed to
      protecting and respecting your privacy through our compliance with this
      policy. This Privacy Policy describes the types of information we may
      collect from you or that you may provide when you visit our website
      located at <a href="http://www.crossstreet.ai/">
        www.crossstreet.ai
      </a>{" "}
      ("Website") or use our Cross Street™ mobile application, a software and
      services platform for real estate agents and the real estate industry
      (collectively the "Platform") and our practices for collecting, using,
      maintaining, protecting and disclosing that information. This Privacy
      Policy applies to information we collect: (i) through our Platform; (ii)
      in e-mail, text, message boards, forums, bulletin boards, and other
      interactive features and other electronic messages between you and our
      Platform and others using our Platform and (iii) through multiple listing
      service (“MLS”) data feeds. This Privacy Policy does not apply to
      information collected by us offline or via third party sites that may be
      accessed through hyperlinks on our Platform.
    </p>
    <p>
      Please read this policy carefully to understand our policies and practices
      regarding your information and how we will treat it. By accessing or using
      our Platform, you agree to this Privacy Policy. IF YOU DO NOT AGREE WITH
      THIS PRIVACY POLICY, PLEASE DO NOT USE OR ACCESS OUR PLATFORM.
    </p>

    <h3>1. INFORMATION WE COLLECT ABOUT YOU AND HOW WE COLLECT IT</h3>
    <p>
      We collect several types of information from and about visitors and/or
      users of our Platform. We may collect this information either directly
      from you when you provide it to us, when we receive information from a MLS
      data feed, or automatically as you navigate through our Platform.
    </p>
    <em>(a) Personal Information.</em>
    <p>
      When you sign up to use our Platform, you will be required to provide us
      with personal information about yourself, including your name, postal
      address, e-mail address, telephone number, MLS identification number and
      real estate license number ("Personal Information"). In addition, we
      receive your email address and telephone number from a MLS data feed.
      Other than as described above, we do not collect Personal Information from
      you when you use the Platform.
    </p>
    <em>(b) User Content.</em>
    <p>
      As part of the use of the Platform, you may post or transmit information,
      text, messages, opinions, commentary, advice, images, photos, video and
      audio or any other materials (“User Content.") This User Content, can
      include, without limitation: information regarding appointments such as
      times, dates, agent or broker names, contact information, listing
      information and property locations; opinions, messages or information
      regarding properties and communications transmitted through the Platform;
      and survey responses or feedback. User Content is posted on the Platform
      and transmitted to other users within the Platform at your own risk.
      Although we limit access to the Platform to registered users of the
      Platform, please be aware that no security measures are perfect or
      impenetrable. Additionally, we cannot control the actions of other
      registered users of the Platform, who will have access to your User
      Content. Therefore, we cannot and do not guarantee that your User Content
      will not be viewed by and distributed to unauthorized persons. You should
      consider any User Content you post to be in the public domain.
    </p>
    <em>(c) Other Information.</em>
    <p>
      In addition to the Personal Information and User Content noted above that
      you voluntarily provide to us or that we receive from a MLS data feed, we
      may use automatic data collection technologies to collect additional
      information (“Behavioral Information”), which may include the following:
      information from your web browser or mobile device about your equipment,
      geographic location, browsing actions and patterns, including details of
      your visits to our Platform, and the resources that you access and use on
      or through the Platform; information about your computer and internet
      connection, including your IP address, operating system and browser type;
      and the servers through which you connect to the Internet. The information
      we collect does not include Personal Information, but we may maintain it
      or associate it with Personal Information we collect in other ways or
      receive from third parties. It helps us to improve our Platform and to
      deliver a better and more personalized service, including by enabling us
      to understand usage patterns so that we can enhance the Platform. We also
      store information about a user’s preferences, allowing us to customize our
      Platform according to a user’s individual interests and enable other users
      to find and recognize you when you return to our Platform.
    </p>

    <em>(d) Information and Tracking Technologies.</em>
    <p>
      The technologies we use for automatic information collection may include:
    </p>
    <ul>
      <li>
        <strong>Cookies (or mobile cookies).</strong> Cookies are small packets
        of data that a website stores on your computer’s hard drive so that your
        computer will remember information about your next visit. We use cookies
        to help us collect information and to enhance your experience using the
        Platform. If you do not want us to place cookies on your hard drive or
        mobile device, you may be able to turn that feature off on your computer
        or mobile device. Please consult your Internet browser’s documentation
        or information on how to do this. However, if you decide not to accept
        cookies from us, the Platform may not function properly.
      </li>
      <li>
        <strong>Web Beacons.</strong> Pages of the Platform and our e-mails may
        contain small electronic files known as web beacons (also referred to as
        clear gifs, pixel tags and single-pixel gifs) that permit us, for
        example, to count users who have visited those pages or opened an e-mail
        and for other related app statistics (for example, verifying system and
        server integrity).
      </li>
    </ul>

    <h3>2. HOW WE USE YOUR INFORMATION</h3>
    <p>
      We use information that we collect about you or that you provide to us as
      follows:
    </p>
    <ul>
      <li>
        To provide our Platform and its contents to you and any other
        information, products or services that you request from us.
      </li>
      <li>
        To market our products as well as those of our affiliates. If you do not
        want us to use your information this way, please check the relevant box
        located on the form when you register or adjust your user preferences in
        your account profile.
      </li>
      <li>To enable other registered users to find and connect with you.</li>
      <li>To provide you with information you request from us.</li>
      <li>To provide you with notices about your account.</li>
      <li>To conduct internal analysis of the Platform.</li>
      <li>
        To carry out our obligations and enforce our rights arising from any
        contracts entered between you and us.
      </li>
      <li>
        To notify you about changes to our Platform, including updates to our
        Platform and any products or services we offer or provide through them.
      </li>
      <li>
        To carry out our obligations and enforce our rights arising from any
        contracts entered between you and us.
      </li>
      <li>
        In any other way we may describe when you provide the information.
      </li>
      <li>For any other purpose with your consent.</li>
    </ul>

    <h3>3. DISCLOSURE OF YOUR INFORMATION</h3>
    <p>
      We will not sell or rent your Personal Information for marketing purposes
      to a third party without your express consent. We will collect, analyze,
      create derivative works, repackage, sell and distribute to our customers
      and third-parties Behavioral Information about our customers' use of the
      Platform. We may disclose aggregated information about our Users, and
      information that does not identify any individual, without restriction. In
      addition, we may disclose Personal Information that we collect or you
      provide as described in this privacy policy as follows: (a) to
      contractors, service providers and other third parties we use to support
      our business; (b) To fulfill the purpose for which you provide it; (c) For
      any other purpose with your consent. We may also disclose your Personal
      Information to your real estate agent or broker or to your
      MLS/Association, or to the real estate agent, broker or MLS/Association of
      the agent with whom you are transacting. Finally, we may disclose your
      Personal Information to comply with any court order, law or legal process,
      or if we believe disclosure is necessary or appropriate to protect our
      rights, property, or safety or that of our registered users or others.
    </p>

    <h3>4. ACCESSING AND CORRECTING YOUR INFORMATION</h3>
    <p>
      You can review and change your personal information by logging into the
      Platform and visiting your account profile page. You may also send us an
      email at <a href="mailto:team@crossstreet.ai">team@crossstreet.ai</a> to
      correct or delete any Personal Information that you have provided to us.
      We cannot delete your Personal Information except by also deleting your
      user account.
    </p>
    <p>
      If you delete your User Content from the Platform, copies of your User
      Content may remain viewable in cached and archived pages, or might have
      been copied or stored by other Platform users. Proper access and use of
      information provided on the Platform, including User Content, is governed
      by our terms of use.
    </p>

    <h3>
      5. ADVERTISING, LINKS TO THIRD PARTY SITES AND THIRD-PARTY INFORMATION
      COLLECTION
    </h3>
    <p>
      We may enter agreements with advertising networks, under which such
      advertising networks may place advertisements on our Platform. We have no
      responsibility for and do not control these advertising networks, the
      websites of third parties reached through links on our Platform or their
      information collection practices. The advertising network and other
      companies with which they work may use cookies and other technologies to
      collect certain non-personally identifiable information when you click on
      the banner ads appearing on our Platform. We have no responsibility for
      the collection or use by such third-parties for such information.
    </p>
    <p>
      Note, that we will not share your Personal Information with third parties
      for their marketing purposes without obtaining your prior consent.
      Moreover, if you have granted us permission, such third parties’ use of
      your Personal Information will be subject to their privacy policies. You
      should contact those entities directly regarding any communications you
      may receive from them, including, if you later decide that you no longer
      want that third party to use your personal information.
    </p>
    <p>
      When you use the Platform or its content, certain third parties may use
      automatic information collection technologies to collect information about
      you or your device. These third parties may include your mobile service
      provider, your mobile device manufacturer, analytics companies and ad
      networks and ad servers. We do not control these third parties tracking
      technologies or how they may be used. If you have any questions about an
      advertisement or other targeted content, you should contact the
      responsible provider directly.
    </p>

    <h3>6. YOUR CALIFORNIA PRIVACY RIGHTS</h3>
    <p>
      If you are a California resident and have provided us with Personal
      Information, California law gives you the right to request and receive
      from us, once per calendar year, information as to how we have shared your
      Personal Information with third parties for their direct marketing
      purposes. If applicable, such information would include a list of names
      and addresses of all third parties with whom such information was shared
      during the prior calendar year as well as a list of the categories of
      Personal Information shared. To make such a request, please send an email
      to team@crossstreetapp.com and include the phrase “California Privacy
      Request” in the subject line. Your request must also include your name,
      physical mailing address and email address. We will respond to you within
      thirty days of receiving such a request.
    </p>

    <h3>7. DATA SECURITY</h3>
    <p>
      All information you provide to us is stored on our secure servers. Access
      by you to your account is available through a password and/or unique user
      name selected by you. We recommend that you do not divulge your password
      to anyone, that you change your password often using a combination of
      letters and numbers, and that you ensure you use a secure web browser when
      applicable. We cannot be held accountable for activity that results from
      your own neglect to safeguard the secrecy of your password and user name.
      Please notify us as soon as possible if your user name or password is
      compromised by emailing{" "}
      <a href="mailto:team@crossstreet.ai">team@crossstreet.ai</a>.
    </p>
    <p>
      Unfortunately, no data transmission over the Internet or any wireless
      network can be guaranteed to be 100% secure. As a result, while we strive
      to protect your Personal Information, you acknowledge that: (a) there are
      security and privacy limitations of the Internet which are beyond our
      control; (b) the security, integrity and privacy of any and all
      information and data exchanged between you and us through our Platform
      cannot be guaranteed and we shall have no liability to you or any third
      party for loss, misuse, disclosure or alteration of such information; and
      (c) any such information and data may be viewed or tampered with in
      transit by a third party.
    </p>
    <p>
      In the unlikely event that we believe that the security of your Personal
      Information in our control may have been compromised, we will notify you
      as promptly as possible under the circumstances. To the extent we have
      your e-mail address, we may notify you by e-mail and you consent to our
      use of e-mail as a means of such notification. If you prefer for us to use
      another method to notify you in this situation, please e-mail us at{" "}
      <a href="mailto:team@crossstreet.ai">team@crossstreet.ai</a> with the
      alternative contact information you wish to be used.
    </p>

    <h3>8. CHANGES TO OUR PRIVACY POLICY</h3>
    <p>
      It is our policy to post any changes we make to our Privacy Policy on this
      page (with a notice that the Privacy Policy has been updated on the home
      page of our website and in our Cross Street mobile application). If we
      make material changes to how we treat our users’ information, we will
      notify you by e-mail to the primary e-mail address specified in your
      account and through a notice on the home pages of our websites. The date
      the Privacy Policy was last revised is identified at the top of the page.
      You are responsible for ensuring we have an up-to-date active and
      deliverable e-mail address for you, and for periodically visiting our
      Platform and this Privacy Policy to check for any changes. Your continued
      use of the Platform after such modifications will constitute your: (a)
      acknowledgment of the modified Privacy Policy; and (b) agreement to abide
      and be bound by the modified Privacy Policy.
    </p>

    <h3>9. CONTACT INFORMATION</h3>
    <p>
      To ask questions or comment about this Privacy Policy and our privacy
      practices, contact us at:
      <a href="mailto:team@crossstreet.ai">team@crossstreet.ai</a> or by sending
      a letter to Avenue Labs, Inc., 124 Olive Ave, Burbank, CA 91502, Attn:
      Support.
    </p>
  </section>
);

export default Privacy;
