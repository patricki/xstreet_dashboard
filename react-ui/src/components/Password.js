import React, { Component } from "react";
import { connect } from "react-redux";
import { changePassword } from "../helpers/http";

class Password extends Component {
  state = {
    successMsg: null
  };

  handleClick = e => {
    e.preventDefault();
    changePassword(this.props.user.email)
      .then(resp => {
        console.log(resp);
        this.setState({ successMsg: resp.data });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    const { user } = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="panel wide">
            <h2>CHANGE PASSWORD</h2>
            <p>To change your password, an email will be sent to:</p>
            <div className="center-block" style={{ marginTop: "1.5rem" }}>
              {this.state.successMsg ? (
                <p>{this.state.successMsg}</p>
              ) : (
                <div className="center-block">
                  <h3>{user.email}</h3>
                  <button
                    className="btn-block"
                    style={{ marginTop: "1.5rem" }}
                    onClick={this.handleClick}
                  >
                    SEND EMAIL
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ user, ui }) => ({
  user,
  ui
});

export default connect(mapStateToProps)(Password);
