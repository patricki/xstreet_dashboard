import React from "react";
import logo from "../assets/logo.png";
import { Route, Router, BrowserRouter, Redirect } from "react-router-dom";

const LoggedOut = () => (
  <main>
    <header className="is-dark">
      <img src={logo} alt="" />
    </header>
    <div className="loggedout">
      <h2>You are logged out.</h2>
      <h2>
        Please{" "}
        <a href="https://avenuelabs.auth0.com/login?client=mAWdn6dcPG4EvQ71quStmCOz6kmJPRD3">
          Login Here
        </a>
      </h2>
    </div>
  </main>
);

export default LoggedOut;
