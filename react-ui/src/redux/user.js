import {
  fetchUserProfile,
  unsubscribePlan,
  reactivatePlan
} from "../helpers/http";
import history from "../history";
import { has } from "lodash";

const AUTH_USER = "AUTH_USER";
const UNAUTH_USER = "UNAUTH_USER";
const FETCHING_USER = "FETCHING_USER";
const FETCHING_USER_FAILURE = "FETCHING_USER_FAILURE";
const FETCHING_USER_SUCCESS = "FETCHING_USER_SUCCESS";
const SETTING_USER_STATE = "SETTING_USER_STATE";
const REMOVE_FETCHING_USER = "REMOVE_FETCHING_USER";

export function fetchUser() {
  return function(dispatch) {
    dispatch({
      type: "IS_LOADING"
    });
    return fetchUserProfile()
      .then(user => {
        return dispatch(setUserStore(user));
      })
      .then(() => history.push("/account"));
  };
}

export function cancelSubscription() {
  return function(dispatch) {
    dispatch({
      type: "IS_LOADING"
    });
    return unsubscribePlan().then(user => {
      console.log("HERES MY RESP USER CANCEL", user);
      return dispatch(setUserStore(user));
    });
  };
}

export function reactivateSubscription() {
  return function(dispatch) {
    dispatch({
      type: "IS_LOADING"
    });
    return reactivatePlan().then(user => {
      console.log("HERES MY RESP USER CANCEL", user);
      return dispatch(setUserStore(user));
    });
  };
}

export function setUserStore(user) {
  console.log("fetchingUserSuccess", user);
  return function(dispatch) {
    dispatch({
      type: FETCHING_USER_SUCCESS,
      payload: {
        user
      }
    });
    dispatch({
      type: "IS_LOADED"
    });
  };
}

const initialState = {
  activeSub: true,
  cancelledSub: false,
  subExists: false
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case AUTH_USER:
      return {
        ...state,
        isAuthed: true,
        authedId: action.uid
      };
    case UNAUTH_USER:
      return {
        ...state,
        isAuthed: false,
        authedId: ""
      };
    case FETCHING_USER:
      return {
        ...state,
        isFetching: true
      };
    case FETCHING_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.error
      };
    case REMOVE_FETCHING_USER:
      return {
        ...state,
        isFetching: false
      };
    case FETCHING_USER_SUCCESS:
      console.log("payload", action.payload.user);
      return {
        ...state,
        ...action.payload.user
      };
    case "SUB":
      return {
        ...state,
        activeSub: !state.activeSub
      };

    default:
      return state;
  }
}
