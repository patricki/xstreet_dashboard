import Auth from "../helpers/Auth";
const auth = new Auth();

const LOG_OUT = "LOG_OUT";
const UNAUTH_USER = "UNAUTH_USER";

export function logoutAndUnauth() {
  return function(dispatch) {
    auth.logout();
    dispatch(unauthUser());
  };
}

export function getSession(authResult) {
  const localToken = localStorage.getItem("id_token");
  return localToken;
}

function unauthUser() {
  return {
    type: UNAUTH_USER
  };
}

const initialState = {
  error: "",
  isAuthed: false,
  authedId: "",
  userInfo: "",
  token: ""
};

export default function authStore(state = initialState, action) {
  switch (action.type) {
    case UNAUTH_USER:
      return {
        ...state,
        isAuthed: false,
        authedId: ""
      };
    default:
      return state;
  }
}
