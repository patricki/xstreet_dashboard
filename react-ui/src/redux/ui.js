const DRAWER = "DRAWER";
const IS_LOADING = "IS_LOADING";
const IS_LOADED = "IS_LOADED";

export function toggleDrawer() {
  return {
    type: "DRAWER"
  };
}

export function dispatchStartLoading() {
  return function(dispatch) {
    return dispatch(startLoading());
  };
}

export function startLoading() {
  console.log("STRT LOADING");
  return {
    type: IS_LOADING
  };
}

export function stopLoading() {
  return {
    type: IS_LOADED
  };
}

const initialState = {
  loading: false,
  drawerOpen: false
};

export default function ui(state = initialState, action) {
  switch (action.type) {
    case IS_LOADING:
      return Object.assign({}, state, {
        loading: true
      });
    case IS_LOADED:
      return Object.assign({}, state, {
        loading: false
      });
    case DRAWER:
      return Object.assign({}, state, {
        drawerOpen: !state.drawerOpen
      });
    default:
      return state;
  }
}
