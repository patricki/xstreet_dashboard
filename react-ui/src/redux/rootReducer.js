import { combineReducers } from "redux";
import user from "./user";
import ui from "./ui";
import auth from "./auth";

const rootReducer = combineReducers({ auth, user, ui });

export default rootReducer;
