import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchUser } from "../redux/user";
import Profile from "../components/Profile/Profile";

class ProfileContainer extends Component {
  render() {
    const { user } = this.props;
    return (
      <div>
        <Profile userProfile={user} />
      </div>
    );
  }
}

const mapStateToProps = ({ user, ui }) => ({
  user,
  ui
});

const mapDispatchToProps = dispatch => ({
  fetchprofile: () => dispatch(fetchUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
