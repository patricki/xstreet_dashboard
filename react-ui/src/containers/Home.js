import React, { Component } from "react";
import { Route } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Nav from "../components/Nav";
import Loader from "../components/Loader/Loader";
import TitleBar from "../components/TitleBar";
import Password from "../components/Password";
import Support from "../components/Support";
import Terms from "../components/Terms";
import Privacy from "../components/Privacy";
import MobileHeader from "../components/MobileHeader";
import Drawer from "../components/Drawer";
import AccountContainer from "../containers/AccountContainer";
import ProfileContainer from "../containers/ProfileContainer";
import { fetchUser } from "../redux/user";
import { toggleDrawer } from "../redux/ui";
import { updateUserProfile } from "../helpers/http";

class Home extends Component {
  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    if (nextProps.ui.drawerOpen) {
      document.body.classList.add("locked");
    }
  }

  toggleMobileDrawer = () => {
    this.props.toggleDrawer();
    this.lockScroll();
  };

  lockScroll() {
    if (!this.props.ui.drawerOpen) {
      document.body.classList.add("locked");
    }
    document.body.classList.remove("locked");
  }

  render() {
    const { ui, user } = this.props;
    return (
      <div className="app-layout">
        <MobileHeader
          drawerOpen={ui.drawerOpen}
          toggleMobileDrawer={this.toggleMobileDrawer}
        />
        <Nav userName={user.displayName} />
        <main>
          <Route component={TitleBar} />
          <section>
            <Route
              path="/profile"
              render={props => <ProfileContainer {...props} />}
            />

            <Route
              path="/account"
              render={props => <AccountContainer {...props} />}
            />
            <Route path="/password" render={props => <Password {...props} />} />
            <Route path="/support" render={props => <Support {...props} />} />
            <Route path="/tos" render={props => <Terms {...props} />} />
            <Route path="/privacy" render={props => <Privacy {...props} />} />
          </section>
        </main>
        <Drawer
          drawerOpen={ui.drawerOpen}
          toggleMobileDrawer={this.toggleMobileDrawer}
        />
        {ui.loading && <Loader />}
      </div>
    );
  }
}

const mapStateToProps = ({ user, ui }) => ({
  user,
  ui
});

const mapDispatchToProps = dispatch => ({
  fetchprofile: () => dispatch(fetchUser()),
  toggleDrawer: () => dispatch(toggleDrawer())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));
