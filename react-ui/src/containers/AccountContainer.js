import React, { Component } from "react";
import { Elements } from "react-stripe-elements";
import Account from "../components/Account/Account";

class AccountContainer extends Component {
  render() {
    return (
      <Elements>
        <Account />
      </Elements>
    );
  }
}
export default AccountContainer;
