import React, { Component } from "react";
import serializeForm from "form-serialize";
import LoginForm from "../components/LoginForm";
import logo from "../assets/logo.png";
// import { testAuth } from "../helpers/http";
import { testLogin } from "../helpers/AuthService";
import axios from "axios";

class Login extends Component {
  handleSubmit = e => {
    e.preventDefault();
    const formValues = serializeForm(e.target, { hash: true });
    console.log("Form Values", formValues);
    testLogin();
  };
  render() {
    return (
      <div>
        <header className="is-dark">
          <img src={logo} alt="" />
        </header>
        <main className="main-login">
          <form onSubmit={this.handleSubmit}>
            <input type="text" name="email" />
            <input type="password" name="password" />
            <input className="input-submit" type="submit" value="SIGN IN" />
          </form>
        </main>
      </div>
    );
  }
}

export default Login;
