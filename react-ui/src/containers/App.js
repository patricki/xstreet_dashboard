import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Home from "../containers/Home";
import LoggedOut from "../components/LoggedOut";

class App extends Component {
  render() {
    const { isAuthenticated } = this.props.auth;
    console.log("IS AUTHED", isAuthenticated());
    return (
      <div>
        {isAuthenticated() && <Home />}

        {!isAuthenticated() && <LoggedOut />}
      </div>
    );
  }
}

export default App;
