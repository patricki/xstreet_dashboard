import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { StripeProvider } from "react-stripe-elements";
import store from "./redux/store";
import { makeMainRoutes } from "./routes";
import registerServiceWorker from "./registerServiceWorker";
import main from "./styles/main.css";

const routes = makeMainRoutes();

ReactDOM.render(
  <Provider store={store}>
    <StripeProvider apiKey="pk_test_ecYcd9aiGzFSmZylTSrPxzjQ">
      {routes}
    </StripeProvider>
  </Provider>,
  document.getElementById("root")
);
