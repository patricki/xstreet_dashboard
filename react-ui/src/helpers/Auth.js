import history from "../history";
import auth0 from "auth0-js";
import { AUTH_CONFIG } from "./auth0-variables";

export default class Auth {
  auth0 = new auth0.WebAuth({
    domain: AUTH_CONFIG.domain,
    clientID: AUTH_CONFIG.clientId,
    redirectUri: AUTH_CONFIG.callbackUrl,
    audience: `https://${AUTH_CONFIG.domain}/userinfo`,
    responseType: "token id_token",
    scope: "openid"
  });

  constructor() {
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
  }

  loginWithCreds() {
    this.auth0.client.login(
      {
        realm: "tests",
        username: "jw@crossstreet.ai",
        password: "apptesting",
        scope: "openid"
      },
      function(err, authResult) {
        console.log(err, authResult);
      }
    );
  }

  login() {
    this.auth0.authorize();
  }

  mockTimeout() {
    console.log("IN mockTimeout");
    setTimeout(history.push("/profile"), 6000);
  }

  handleAuthentication(token) {
    console.log("handleAuthentication called", token, this.auth0.parseHash);
    this.auth0.parseHash(window.location.hash, (authResult, err) => {
      console.log("handleAuthentication log authResult", authResult);
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        history.push("/profile");
      } else if (err) {
        console.log(err);
        //alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  setSession(authResult) {
    console.log("setSession called", authResult);
    // Set the time that the access token will expire at
    let expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    const scopes = authResult.scope || this.requestedScopes || "";
    localStorage.setItem("access_token", authResult.accessToken);
    localStorage.setItem("id_token", authResult.idToken);
    localStorage.setItem("expires_at", expiresAt);
    localStorage.setItem("scopes", JSON.stringify(scopes));
    // navigate to the home route
    history.push("/profile");
  }

  logout() {
    console.log("LOGOUT", this.isAuthenticated());
    // Clear access token and ID token from local storage
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    // navigate to the home route
    history.push("/login");
  }

  isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    let expiresAt = JSON.parse(localStorage.getItem("expires_at"));
    return new Date().getTime() < expiresAt;
  }
}
