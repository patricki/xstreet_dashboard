import axios from "axios";
import { AUTH_CONFIG } from "./auth0-variables";
import { setUserStore } from "../redux/user";
import store from "../redux/store";

const BASE_API_URL = "https://qa.aws.avenue.la";
const BASE_AUTH_URL = "https://avenuelabs.auth0.com";
const AUTH_TOKEN = localStorage.getItem("id_token");
//axios.defaults.headers.common["authorization"] = `Bearer ${AUTH_TOKEN}`;
// console.log(axios.defaults.headers.common["authorization"]);

export async function fetchUserProfile() {
  const token = localStorage.getItem("id_token");
  try {
    const profile = await axios.get(`${BASE_API_URL}/user/v1/users/user`, {
      headers: {
        authorization: `Bearer ${token}`
      }
    });
    return profile.data;
  } catch (error) {
    console.log("CATCH ERR", error);
  }
}

export async function unsubscribePlan() {
  const token = localStorage.getItem("id_token");
  try {
    const profile = await axios.post(
      `${BASE_API_URL}/user/v1/users/unsubscribe`,
      {},
      {
        headers: {
          authorization: `Bearer ${token}`
        }
      }
    );
    console.log("unsubscribePlan", profile.data);
    return profile.data;
  } catch (error) {
    console.log("CATCH ERR", error);
  }
}

export async function reactivatePlan() {
  const token = localStorage.getItem("id_token");
  try {
    const profile = await axios.post(
      `${BASE_API_URL}/user/v1/users/subscription/reactivate`,
      {},
      {
        headers: {
          authorization: `Bearer ${token}`
        }
      }
    );
    console.log("reactivatePlan", profile.data);
    return profile.data;
  } catch (error) {
    console.log("CATCH ERR", error);
  }
}

export async function sendStripePayment(paymentTok, planId) {
  const token = localStorage.getItem("id_token");
  store.dispatch({ type: "IS_LOADING" });
  //console.log("EXTERNAL STORE", store.getState().user.externalPaymentToken);
  try {
    const profile = await axios.put(
      `${BASE_API_URL}/user/v1/users/user`,
      {
        externalPaymentToken: paymentTok,
        externalPaymentPlanId: planId
      },
      {
        headers: {
          authorization: `Bearer ${token}`
        }
      }
    );
    //console.log("stripe pay response", profile.data.externalPaymentToken);
    store.dispatch(setUserStore(profile.data));
  } catch (error) {
    store.dispatch({ type: "IS_LOADED" });
    console.log("CATCH ERR", error);
  }
}

export function changePassword(email) {
  return axios.post(`${BASE_AUTH_URL}/dbconnections/change_password`, {
    client_id: AUTH_CONFIG.clientId,
    email: email,
    connection: "Username-Password-Authentication"
  });
}

// export async function updateUserProfile(profile) {
//   try {
//     const profile = await axios.put(`${BASE_API_URL}/user/v1/users/user`, {
//       brokerage: "new brokerage"
//     });
//     return profile;
//   } catch (error) {
//     console.log("CATCH ERR", error);
//   }
// }
