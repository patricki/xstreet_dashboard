import Auth0Lock from "auth0-lock";
import history from "../history";
import { startLoading, stopLoading } from "../redux/ui";
import { fetchUser } from "../redux/user";
import store from "../redux/store";
var clientId = "mAWdn6dcPG4EvQ71quStmCOz6kmJPRD3";
var domain = "avenuelabs.auth0.com";
// Instantiate Lock - without custom options

const lockOptions = {
  allowSignUp: false,
  allowShowPassword: true,
  closable: false,
  theme: {
    logo: "http://xstreet.screengroove.com/images/auth0-logo.png",
    primaryColor: "#377bff"
  },
  languageDictionary: {
    title: "Cross Street Dashboard"
  }
};
var lock = new Auth0Lock(clientId, domain, lockOptions);

// Listen for the authenticated event and get profile
lock.on("authenticated", function(authResult) {
  console.log("lock on event", authResult);
  lock.getUserInfo(authResult.accessToken, function(error, profile) {
    if (error) {
      console.log("lock on err", error);
      return;
    }

    //Save token and profile locally
    let expiresAt = JSON.stringify(
      authResult.idTokenPayload.exp * 1000 + new Date().getTime()
    );
    localStorage.setItem("accessToken", authResult.accessToken);
    localStorage.setItem("expires_at", expiresAt);
    localStorage.setItem("id_token", authResult.idToken);
    localStorage.setItem("profile", JSON.stringify(profile));
    getUserOnLogin();
    //history.push("/callback");
  });
});

function getUserOnLogin() {
  store.dispatch(fetchUser()).then(console.log("THEN"));
}

lock.on("authorization_error", function(error) {
  console.log("authorization_error", error);
});

export default lock;
