import format from "date-fns/format";
import differenceInDays from "date-fns/difference_in_days";
import parse from "date-fns/parse";

export const convertDate = (timestamp, formatStyle) => {
  let date = new Date(timestamp);
  return format(date, formatStyle);
};

export const convertDaysLeft = timestamp => {
  return differenceInDays(parse(timestamp), new Date());
};
